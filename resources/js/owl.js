require('owl.carousel');
$(document).ready(function () {


    var owl = $('.owl-loop-products').owlCarousel({
        center: false,
        items: 1,
        loop: true,
        stagePadding: 0,
        lazyLoad: true,
        //smartSpeed: 1000,
        autoplay: true,
        autoplayHoverPause: true,


        responsive: {
            300: {
                margin: 20,
                nav: false,
                items: 2
            },
            600: {
                margin: 20,
                nav: false,
                items: 2
            },
            1000: {
                margin: 20,
                stagePadding: 0,
                nav: false,
                items: 3
            },
            1200: {
                margin: 20,
                stagePadding: 0,
                nav: false,
                items: 5
            }
        }
    });

    var owl = $('.owl-loop-cats').owlCarousel({
        center: false,
        items: 1,
        loop: true,

        lazyLoad: true,
        autoplay: true,
        autoplayHoverPause: true,

        responsive: {
            300: {
                margin: 20,
                nav: false,
                items: 2
            },
            600: {
                margin: 20,
                nav: false,
                items: 2
            },
            1000: {
                margin: 20,

                nav: false,
                items: 3
            },
            1200: {
                margin: 40,

                nav: false,
                items: 6
            }
        }
    });


/*
    $('.owl-next').click(function () {
        owl.trigger('next.owl.carousel');
    })
    $('.owl-prev').click(function () {
        owl.trigger('prev.owl.carousel');
    })
*/
});
