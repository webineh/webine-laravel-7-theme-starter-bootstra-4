<!doctype html>
<html lang="fa" dir="rtl">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{mix('css/app.css') }}" rel="stylesheet">
</head>
<body>

@include('include.header')
<main>
    @yield('content')
</main>
@include('include.footer')

<script src="{{ mix('js/app.js') }}"></script>
</body>
</html>
